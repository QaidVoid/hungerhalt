enum ApprovalStatus {
  Pending = "Pending",
  Accepted = "Accepted",
  Rejected = "Rejected"
}

enum UserRole {
}

type RequestStatus = ApprovalStatus;

export type User = {
  id: number;
  name: string;
  email: string;
  password: string;
  role: UserRole
}

export type FoodRequest = {
  id: number;
  donation: number;
  quantity: number;
  requestedAt: Date;
  status: RequestStatus;
  userId: number;
  foodId: number;
  user: User;
}


export type Food = {
  id: number;
  name: string;
  quantity: number;
  postedAt: Date;
  expiresAt: Date;
  pickupLocation: string;
  approvalStatus: ApprovalStatus,
  userId: number;
}

export type FoodRequestWithFood = FoodRequest & {
  food: Food;
}

export type FoodWithRequest = Food & {
  request: FoodRequest[];
}

export type FoodWithUser = Food & {
  user: User
}

export type NewFood = {
  name: string;
  quantity: number;
  expiresAt: Date;
  pickupLocation: string;
}

export type AuthUser = {
  id: number;
  role: string;
}

export type LoginUser = {
  email: string;
  password: string;
}

export type RegisterUser = LoginUser & {
  name: string;
}

export type CreateFoodRequest = {
  donation: number;
  quantity: number;
}
