import { useState } from 'react';
import { Tabs } from '@mantine/core';
import MyRequests from '../components/my-requests';
import ManageRequests from '../components/manage-requests';

export default function FoodRequests() {
  const [activeTab, setActiveTab] = useState<string | null>('first');

  return (
    <Tabs value={activeTab} onChange={setActiveTab}>
      <Tabs.List>
        <Tabs.Tab value="first">My Requests</Tabs.Tab>
        <Tabs.Tab value="second">Manage Requests</Tabs.Tab>
      </Tabs.List>

      <Tabs.Panel value="first">
        <MyRequests />
      </Tabs.Panel>
      <Tabs.Panel value="second">
        <ManageRequests />
      </Tabs.Panel>
    </Tabs>
  );
}
