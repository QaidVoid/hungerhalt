import { useCallback, useEffect, useState } from 'react';
import { Table, Button, Paper } from '@mantine/core';
import { IconEdit } from "@tabler/icons-react";
import EditFood from '../components/edit-food';
import CreateFood from '../components/create-food';
import type { FoodWithRequest } from '../types';
import { api } from '../api';

export default function ManageFoods() {
  const [selectedRow, setSelectedRow] = useState<number>(-1);
  const [addFood, setAddFood] = useState<boolean>(false);
  const [foods, setFoods] = useState<FoodWithRequest[]>([]);

  const fetchFoods = useCallback(async () => {
    const res = await api.fetch("/api/food/mine");
    if (res.ok) {
      const data = await res.json();
      setFoods(data.map(v => ({
        ...v,
        expiresAt: new Date(v.expiresAt)
      })));
    }
  }, [])

  useEffect(() => {
    fetchFoods();
  }, [fetchFoods]);

  const rows = foods.map((element) => (
    <Table.Tr
      key={element.id}
    >
      <Table.Td>{element.name}</Table.Td>
      <Table.Td>{element.quantity}</Table.Td>
      <Table.Td>{element.approvalStatus}</Table.Td>
      <Table.Td>{element.expiresAt.toLocaleDateString()}</Table.Td>
      <Table.Td>{element.request.length}</Table.Td>

      <Table.Td>
        <Button onClick={(e) => {
          e.preventDefault();
          setSelectedRow(element.id)
        }}>
          <IconEdit />
        </Button>
      </Table.Td>
    </Table.Tr>
  ));

  return (
    <Paper>
      <Paper py="md">
        <Button onClick={() => { setAddFood(true) }}>Add New Food</Button>
      </Paper>

      <CreateFood opened={addFood} close={() => setAddFood(false)} />

      <EditFood food={foods.find(e => e.id === selectedRow)} opened={selectedRow > -1} close={() => {
        setSelectedRow(-1)
      }} />

      <Paper p="md" shadow="xs">
        <Table striped>
          <Table.Thead>
            <Table.Tr>
              <Table.Th>Name</Table.Th>
              <Table.Th>Quantity</Table.Th>
              <Table.Th>Status</Table.Th>
              <Table.Th>Expires At</Table.Th>
              <Table.Th>Requests</Table.Th>
              <Table.Th />
            </Table.Tr>
          </Table.Thead>
          <Table.Tbody>{rows}</Table.Tbody>
        </Table>
      </Paper>
    </Paper>
  );
}
