import { Center, Paper, Text } from "@mantine/core";
import FoodCard from "../components/food-card";
import { useCallback, useEffect, useState } from "react";
import type { FoodWithUser } from "../types";
import { api } from "../api";
import RequestFood from "../components/request-food";

const Foods = () => {
  const [foodData, setFoodData] = useState<FoodWithUser[]>([]);
  const [selectedFood, setSelectedFood] = useState<number>(-1);

  const fetchFoods = useCallback(async () => {
    const res = await api.fetch("/api/food/recent");
    if (res.ok) {
      const data = await res.json();
      setFoodData(
        data.map((v) => ({
          ...v,
          postedAt: new Date(v.postedAt),
          expiresAt: new Date(v.expiresAt),
        })),
      );
    }
  }, []);

  useEffect(() => {
    fetchFoods();
  }, [fetchFoods]);

  return (
    <>
      <Text className="text-center font-semibold text-3xl py-4">Featured Foods</Text>
      <RequestFood
        food={foodData.find((f) => f.id === selectedFood)}
        opened={selectedFood > -1}
        close={() => setSelectedFood(-1)}
      />
      <Paper shadow="xs" className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4 pe-4">
        {foodData.map((food, index) => (
          <FoodCard key={index} food={food} setSelectedFood={setSelectedFood} />
        ))}
      </Paper>
      {foodData.length < 1 && (
        <Center>
          <Text>No foods to show. Please check later.</Text>
        </Center>
      )}
    </>
  );
};

export default Foods;
