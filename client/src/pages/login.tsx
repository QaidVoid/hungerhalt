import { useToggle, upperFirst } from "@mantine/hooks";
import { useForm } from "@mantine/form";
import {
  TextInput,
  PasswordInput,
  Text,
  Paper,
  Group,
  type PaperProps,
  Button,
  Checkbox,
  Anchor,
  Stack,
  Center,
  Container,
} from "@mantine/core";
import type { LoginUser, RegisterUser } from "../types";
import { api } from "../api";
import { toast } from "react-toastify";
import { setCookie } from "../utils";
import { useAuth } from "../context/auth";
import { useLocation, useNavigate } from "react-router-dom";

export default function Authenticate(props: PaperProps) {
  const [type, toggle] = useToggle(["login", "register"]);
  const form = useForm({
    initialValues: {
      email: "",
      name: "",
      password: "",
      terms: true,
    },

    validate: {
      email: (val) => (/^\S+@\S+$/.test(val) ? null : "Invalid email"),
      password: (val) => (val.length <= 6 ? "Password should include at least 6 characters" : null),
    },
  });

  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || "/";
  const auth = useAuth();

  const handleSubmit = async (data: LoginUser | RegisterUser) => {
    const isRegisterUser = (user: LoginUser | RegisterUser): user is RegisterUser => {
      return (user as RegisterUser).name !== undefined;
    };

    if (type === "login") {
      try {
        const res = await api.post("/api/auth/login", {
          body: JSON.stringify({
            email: data.email,
            password: data.password,
          }),
        });

        if (res.ok) {
          const data = await res.json();
          setCookie("token", data.token, 14 * 86400);
          auth.login(data.user);
          toast.success("Login success.");
          if (data.user.role === "Admin" && !from.startsWith("/admin")) {
            navigate("/admin/foods", { replace: true });
          } else if (data.user.role === "User" && from.startsWith("/admin")) {
            navigate("/foods", { replace: true });
          } else {
            navigate(from, { replace: true });
          }
        } else {
          toast.error("Invalid credentials.");
        }
      } catch (error) {
        console.log("Login error:", error);
        toast.error("Internal server error");
      }
    } else if (isRegisterUser(data)) {
      try {
        const res = await api.post("/api/auth/register", {
          body: JSON.stringify({
            email: data.email,
            name: data.name,
            password: data.password,
          }),
        });

        if (res.ok) {
          const data = await res.json();
          setCookie("token", data.token, 14 * 86400);
          auth.login(data.user);
          toast.success("Register success");
          navigate(from, { replace: true });
        } else {
          toast.error("Register error");
        }
      } catch (error) {
        console.log("Register error:", error);
        toast.error("Internal server error");
      }
    }
  };

  return (
    <Container size="sm" mt="14">
      <Paper radius="md" p="xl" withBorder {...props}>
        <Center>
          <Text size="xl" fw={700}>
            Welcome to HungerHalt
          </Text>
        </Center>

        <form onSubmit={form.onSubmit(handleSubmit)}>
          <Stack>
            {type === "register" && (
              <TextInput
                required
                label="Name"
                placeholder="Your name"
                value={form.values.name}
                onChange={(event) => form.setFieldValue("name", event.currentTarget.value)}
                radius="md"
              />
            )}

            <TextInput
              required
              label="Email"
              placeholder="hello@example.com"
              value={form.values.email}
              onChange={(event) => form.setFieldValue("email", event.currentTarget.value)}
              error={form.errors.email && "Invalid email"}
              radius="md"
            />

            <PasswordInput
              required
              label="Password"
              placeholder="Your password"
              value={form.values.password}
              onChange={(event) => form.setFieldValue("password", event.currentTarget.value)}
              error={form.errors.password && "Password should include at least 6 characters"}
              radius="md"
            />

            {type === "register" && (
              <Checkbox
                label="I accept terms and conditions"
                checked={form.values.terms}
                onChange={(event) => form.setFieldValue("terms", event.currentTarget.checked)}
              />
            )}
          </Stack>

          <Group justify="space-between" mt="xl">
            <Anchor component="button" type="button" c="dimmed" onClick={() => toggle()} size="md">
              {type === "register"
                ? "Already have an account? Login"
                : "Don't have an account? Register"}
            </Anchor>
            <Button type="submit" radius="xl">
              {upperFirst(type)}
            </Button>
          </Group>
        </form>
      </Paper>
    </Container>
  );
}
