export const api = {
  post: (endpoint: string, init?: RequestInit) => {
    const headers = {
      ...init?.headers,
      "Content-Type": "application/json",
    };
    return fetch(endpoint, {
      ...init,
      method: "POST",
      credentials: "include",
      headers,
    });
  },
  put: (endpoint: string, init?: RequestInit) => {
    const headers = {
      ...init?.headers,
      "Content-Type": "application/json",
    };
    return fetch(endpoint, {
      ...init,
      method: "PUT",
      credentials: "include",
      headers,
    });
  },
  fetch: (endpoint: string, init?: RequestInit) =>
    fetch(endpoint, {
      ...init,
      credentials: "include",
    }),
};
