import { Group, Button, Divider, Box, Burger, Drawer, Text, ScrollArea, rem } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import classes from "./header.module.css";
import { Link, useNavigate } from "react-router-dom";
import { useAuth } from "../context/auth";

const links = [
  { link: "/foods", label: "Foods" },
  { link: "/manage-foods", label: "Manage Foods" },
  { link: "/requests", label: "Requests" },
];

export default function Navbar() {
  const [drawerOpened, { toggle: toggleDrawer, close: closeDrawer }] = useDisclosure(false);
  const auth = useAuth();
  const navigate = useNavigate();

  return (
    <Box pb={4}>
      <header className={classes.header}>
        <Group justify="space-between" h="100%">
          <Text>HungerHalt</Text>

          <Group h="100%" gap={0} visibleFrom="sm">
            {links.map((link, idx) => (
              <Link key={idx} to={link.link} className={classes.link}>
                {link.label}
              </Link>
            ))}
          </Group>

          <Group visibleFrom="sm">
            {auth.authenticated ? (
              <Button
                color="red"
                onClick={() => {
                  auth.logout();
                }}
              >
                Log out
              </Button>
            ) : (
              <Button variant="default" onClick={() => navigate("/login")}>
                Log in
              </Button>
            )}
          </Group>

          <Burger opened={drawerOpened} onClick={toggleDrawer} hiddenFrom="sm" />
        </Group>
      </header>

      <Drawer
        opened={drawerOpened}
        onClose={closeDrawer}
        size="100%"
        padding="md"
        title="Navigation"
        hiddenFrom="sm"
        zIndex={1000000}
      >
        <ScrollArea h={`calc(100vh - ${rem(80)})`} mx="-md">
          <Divider my="sm" />

          {links.map((link, idx) => (
            <Link key={idx} to={link.link} className={classes.link}>
              {link.label}
            </Link>
          ))}

          <Divider my="sm" />

          <Group justify="center" grow pb="xl" px="md">
            {auth.authenticated ? (
              <Button
                color="red"
                onClick={() => {
                  auth.logout();
                }}
              >
                Log out
              </Button>
            ) : (
              <Button variant="default">Log in</Button>
            )}
          </Group>
        </ScrollArea>
      </Drawer>
    </Box>
  );
}
