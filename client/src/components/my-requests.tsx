import { useCallback, useEffect, useState } from "react";
import { Table, Button, Paper } from "@mantine/core";
import { IconTrash } from "@tabler/icons-react";
import type { FoodRequestWithFood } from "../types";
import { api } from "../api";
import { toast } from "react-toastify";
import dayjs from "dayjs";

export default function MyRequests() {
  const [foodRequests, setFoodRequests] = useState<FoodRequestWithFood[]>([]);

  const fetchRequests = useCallback(async () => {
    const res = await api.fetch("/api/request");
    if (res.ok) {
      const data = await res.json();
      setFoodRequests(
        data.map((v) => ({
          ...v,
          requestedAt: new Date(v.requestedAt),
        })),
      );
    }
  }, []);

  useEffect(() => {
    fetchRequests();
  }, [fetchRequests]);

  const rows = foodRequests.map((element) => (
    <Table.Tr key={element.id}>
      <Table.Td>{element.id}</Table.Td>
      <Table.Td>{element.food.name}</Table.Td>
      <Table.Td>{element.quantity}</Table.Td>
      <Table.Td>${element.donation}</Table.Td>
      <Table.Td>{element.status}</Table.Td>
      <Table.Td>{dayjs(element.requestedAt).format("YYYY/MM/DD HH:MM:ss")}</Table.Td>

      <Table.Td>
        <Button
          px="sm"
          disabled={element.status === "Accepted"}
          color="red"
          onClick={async (e) => {
            e.preventDefault();

            try {
              const res = await api.put(`/api/request/accept/${element.id}`);
              if (res.ok) {
                toast.success("Request accepted");
              } else {
                toast.error("Error accepting request");
              }
            } catch {
              toast.error("Internal server error");
            }
          }}
        >
          <IconTrash color="white" />
        </Button>
      </Table.Td>
    </Table.Tr>
  ));

  return (
    <Paper>
      <Paper p="md" shadow="xs">
        <Table striped>
          <Table.Thead>
            <Table.Tr>
              <Table.Th>ID</Table.Th>
              <Table.Th>Food Name</Table.Th>
              <Table.Th>Quantity</Table.Th>
              <Table.Th>Donation</Table.Th>
              <Table.Th>Status</Table.Th>
              <Table.Th>Requested At</Table.Th>
              <Table.Th />
            </Table.Tr>
          </Table.Thead>
          <Table.Tbody>{rows}</Table.Tbody>
        </Table>
      </Paper>
    </Paper>
  );
}
