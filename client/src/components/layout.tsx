import { Container } from "@mantine/core";
import Navbar from "./navbar";
import { Outlet } from "react-router-dom";

const Layout = () => {
  return (
    <Container size="lg" className="p-4">
      <Navbar />
      <Outlet />
    </Container>
  );
};

export default Layout;
