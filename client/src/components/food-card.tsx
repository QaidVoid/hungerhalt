import { Card, Text, Avatar, Button } from "@mantine/core";
import { IconCalendar, IconCheck, IconLocation } from "@tabler/icons-react";
import type { FoodWithUser } from "../types";
import { useAuth } from "../context/auth";

type Props = {
  food: FoodWithUser,
  setSelectedFood: React.Dispatch<React.SetStateAction<number>>
}

const FoodCard = ({ food, setSelectedFood }: Props) => {
  const auth = useAuth();

  return (
    <>
      <Card shadow="xs" padding="md" className="w-full m-2">
        <Text size="xl">{food.name}</Text>
        <div className="flex items-center mt-1">
          <IconCheck size={20} />
          <Text size="sm" className="text-gray-500 ml-1">
            Quantity: {food.quantity}
          </Text>
        </div>

        <div className="flex items-center mt-1">
          <IconCalendar size={20} />
          <Text size="sm" className="text-gray-500 ml-1">
            Posted on: {new Date(food.postedAt).toLocaleDateString()}
          </Text>
        </div>
        <div className="flex items-center mt-1">
          <IconCalendar size={20} />
          <Text size="sm" className="text-gray-500 ml-1">
            Expires at: {new Date(food.expiresAt).toLocaleDateString()}
          </Text>
        </div>
        <div className="flex items-center mt-1">
          <IconLocation size={20} />
          <Text size="sm" className="text-gray-500 ml-1">
            Location: {food.pickupLocation}
          </Text>
        </div>
        <div className="flex items-center mt-2">
          <Avatar size="sm" />
          <Text size="sm" className="text-gray-500 ml-1">
            {food.user.name}
          </Text>
        </div>
        {food.userId === auth.user.id ? <Button disabled fullWidth className="mt-2">Added by you</Button> :
          <Button fullWidth className="mt-2" onClick={() => setSelectedFood(food.id)}>
            Request Food
          </Button>
        }
      </Card>
    </>
  );
};

export default FoodCard;
