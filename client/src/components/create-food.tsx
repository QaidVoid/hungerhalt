import { FileInput, Modal, NumberInput } from "@mantine/core";
import { Button, TextInput } from "@mantine/core";
import { hasLength, useForm } from "@mantine/form";
import { DateInput } from "@mantine/dates";
import type { NewFood } from "../types";
import { api } from "../api";
import { toast } from "react-toastify";

const CreateFood = ({ opened, close }) => {
  const form = useForm<NewFood>({
    mode: "uncontrolled",
    validate: {
      name: hasLength({ min: 3 }, "Must be at least 3 characters"),
      pickupLocation: hasLength({ min: 3 }, "Must be at least 3 characters"),
    },
  });

  const handleSubmit = async (data: NewFood) => {
    try {
      const res = await api.post("/api/food", {
        body: JSON.stringify({
          name: data.name,
          quantity: data.quantity,
          pickupLocation: data.pickupLocation,
          expiresAt: data.expiresAt,
        }),
      });

      if (res.ok) {
        toast.success("Food added.");
        close();
      } else {
        throw new Error();
      }
    } catch (error) {
      toast.error("Failed to add food.");
      console.log("ERROR:", error);
    }
  };

  return (
    <>
      <Modal
        opened={opened}
        onClose={close}
        title="Add Food"
        overlayProps={{
          backgroundOpacity: 0.55,
          blur: 3,
        }}
      >
        <form encType="multipart/form-data" onSubmit={form.onSubmit(handleSubmit)}>
          <TextInput
            {...form.getInputProps("name")}
            key={form.key("name")}
            label="Name"
            placeholder="Name"
          />
          <NumberInput
            {...form.getInputProps("quantity")}
            allowNegative={false}
            allowDecimal={false}
            key={form.key("quantity")}
            mt="md"
            label="Quantity"
            placeholder="Quantity"
          />
          <DateInput
            {...form.getInputProps("expiresAt")}
            allowDeselect={true}
            key={form.key("expiresAt")}
            mt="md"
            label="Expires At"
            placeholder="Expiry"
          />
          <TextInput
            {...form.getInputProps("pickupLocation")}
            key={form.key("pickupLocation")}
            mt="md"
            label="Pickup Location"
            placeholder="Pickup Location"
          />
          <FileInput
            {...form.getInputProps("image")}
            clearable
            accept="image/*"
            key={form.key("image")}
            mt="md"
            label="Image"
            placeholder="Image"
          />
          <Button type="submit" mt="md">
            Submit
          </Button>
        </form>
      </Modal>
    </>
  );
};

export default CreateFood;
