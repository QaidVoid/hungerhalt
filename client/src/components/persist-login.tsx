import { useEffect } from "react";
import { useAuth } from "../context/auth";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import { api } from "../api";

const PersistLogin = () => {
  const auth = useAuth();
  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || "/";

  useEffect(() => {
    const verifyAuth = async () => {
      try {
        const res = await api.fetch("/api/auth/verify");
        if (res.ok) {
          const user = await res.json();
          auth.login(user);
        }
      } catch (err) {
        console.error(err);
        auth.logout();
      }
    };
    !auth.authenticated ? verifyAuth() : null;
  }, [auth]);

  useEffect(() => {
    if (auth.authenticated && from === "/login") {
      navigate("/foods");
    }
    if (auth.authenticated && auth.user.role === "Admin" && !from.startsWith("/admin")) {
      navigate("/admin/foods", { replace: true });
    } else if (auth.authenticated && auth.user.role === "Admin" && from.startsWith("/admin")) {
      navigate(from, { replace: true });
    }
  }, [auth, from, navigate]);

  return <Outlet />;
};

export default PersistLogin;
