import { useCallback, useEffect, useState } from "react";
import { Table, Paper, Text, Center } from "@mantine/core";
import type { User } from "../../types";
import { api } from "../../api";

export default function Users() {
  const [foods, setFoods] = useState<User[]>([]);

  const fetchFoods = useCallback(async () => {
    const res = await api.fetch("/api/admin/users/all");
    if (res.ok) {
      const data = await res.json();
      setFoods(
        data.map((v) => ({
          ...v,
          postedAt: new Date(v.postedAt),
          expiresAt: new Date(v.expiresAt),
        })),
      );
    }
  }, []);

  useEffect(() => {
    fetchFoods();
  }, [fetchFoods]);

  const rows = foods.map((element) => (
    <Table.Tr key={element.id}>
      <Table.Td>{element.name}</Table.Td>
      <Table.Td>{element.email}</Table.Td>
      <Table.Td>{element.role}</Table.Td>
    </Table.Tr>
  ));

  return (
    <Paper py="md">
      <Center py="md">
        <Text className="font-semibold text-2xl">Manage Foods</Text>
      </Center>
      <Paper p="md" shadow="xs">
        <Table striped>
          <Table.Thead>
            <Table.Tr>
              <Table.Th>Name</Table.Th>
              <Table.Th>Email</Table.Th>
              <Table.Th>Role</Table.Th>
              <Table.Th />
            </Table.Tr>
          </Table.Thead>
          <Table.Tbody>{rows}</Table.Tbody>
        </Table>
      </Paper>
    </Paper>
  );
}
