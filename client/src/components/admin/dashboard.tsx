import { Group, Text } from "@mantine/core";
import { IconLogout, IconBread, IconUsers } from "@tabler/icons-react";
import classes from "./dashboard.module.css";
import { NavLink, Outlet } from "react-router-dom";
import { useAuth } from "../../context/auth";

const data = [
  { link: "/admin/foods", label: "Foods", icon: IconBread },
  { link: "/admin/users", label: "Users", icon: IconUsers },
];

export default function Dashboard() {
  const auth = useAuth();

  const links = data.map((item) => (
    <NavLink
      className={({ isActive }) => (isActive ? `${classes.link} bg-gray-100` : `${classes.link}`)}
      to={item.link}
      key={item.label}
    >
      <item.icon className={classes.linkIcon} stroke={1.5} />
      <span>{item.label}</span>
    </NavLink>
  ));

  return (
    <div className="flex">
      <nav className={classes.navbar}>
        <div className={classes.navbarMain}>
          <Group className={classes.header} justify="space-between">
            <Text>HungerHalt</Text>
          </Group>
          {links}
        </div>

        <div className={classes.footer}>
          <a className={classes.link} onClick={() => auth.logout()}>
            <IconLogout className={classes.linkIcon} stroke={1.5} />
            <span>Logout</span>
          </a>
        </div>
      </nav>
      <div className="px-4 py-2 flex-1">
        <Outlet />
      </div>
    </div>
  );
}
