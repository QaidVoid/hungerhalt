import { useCallback, useEffect, useState } from "react";
import { Table, Button, Paper, Text, Center } from "@mantine/core";
import { IconCheckbox, IconX } from "@tabler/icons-react";
import type { FoodWithRequest } from "../../types";
import { api } from "../../api";
import { toast } from "react-toastify";
import dayjs from "dayjs";

export default function UserFoods() {
  const [foods, setFoods] = useState<FoodWithRequest[]>([]);

  const fetchFoods = useCallback(async () => {
    const res = await api.fetch("/api/admin/food/all");
    if (res.ok) {
      const data = await res.json();
      setFoods(
        data.map((v) => ({
          ...v,
          postedAt: new Date(v.postedAt),
          expiresAt: new Date(v.expiresAt),
        })),
      );
    }
  }, []);

  useEffect(() => {
    fetchFoods();
  }, [fetchFoods]);

  const rows = foods.map((element) => (
    <Table.Tr key={element.id}>
      <Table.Td>{element.name}</Table.Td>
      <Table.Td>{element.quantity}</Table.Td>
      <Table.Td>{element.approvalStatus}</Table.Td>
      <Table.Td>{dayjs(element.postedAt).format("YYYY/MM/DD HH:MM:ss")}</Table.Td>
      <Table.Td>{dayjs(element.expiresAt).format("YYYY/MM/DD HH:MM:ss")}</Table.Td>

      <Table.Td>
        <Button
          mx="sm"
          px="sm"
          color="green"
          disabled={element.approvalStatus !== "Pending"}
          onClick={async (e) => {
            e.preventDefault();

            try {
              const res = await api.put(`/api/admin/food/${element.id}`, {
                body: JSON.stringify({
                  approvalStatus: "Accepted"
                })
              });
              if (res.ok) {
                toast.success("Food accepted");
              } else {
                toast.error("Error accepting food");
              }
            } catch {
              toast.error("Internal server error");
            }
          }}
        >
          <IconCheckbox color="white" />
        </Button>

        <Button
          mx="sm"
          px="sm"
          color="red"
          disabled={element.approvalStatus !== "Pending"}
          onClick={async (e) => {
            e.preventDefault();

            try {
              const res = await api.put(`/api/admin/food/${element.id}`, {
                body: JSON.stringify({
                  approvalStatus: "Rejected"
                })
              });
              if (res.ok) {
                toast.success("Food rejected");
              } else {
                toast.error("Error rejecting food");
              }
            } catch {
              toast.error("Internal server error");
            }
          }}
        >
          <IconX color="white" />
        </Button>
      </Table.Td>
    </Table.Tr>
  ));

  return (
    <Paper py="md">
      <Center py="md">
        <Text className="font-semibold text-2xl">Manage Foods</Text>
      </Center>
      <Paper p="md" shadow="xs">
        <Table striped>
          <Table.Thead>
            <Table.Tr>
              <Table.Th>Name</Table.Th>
              <Table.Th>Quantity</Table.Th>
              <Table.Th>Status</Table.Th>
              <Table.Th>Posted At</Table.Th>
              <Table.Th>Expires At</Table.Th>
              <Table.Th />
            </Table.Tr>
          </Table.Thead>
          <Table.Tbody>{rows}</Table.Tbody>
        </Table>
      </Paper>
    </Paper>
  );
}
