import { Modal, NumberInput, TextInput } from "@mantine/core";
import { useEffect } from "react";
import { Button } from "@mantine/core";
import { useForm } from "@mantine/form";
import type { CreateFoodRequest, FoodWithUser } from "../types";
import { api } from "../api";
import { toast } from "react-toastify";

type Props = {
  food: FoodWithUser;
  opened: boolean;
  close: () => unknown;
};

const RequestFood = ({ food, opened, close }: Props) => {
  const form = useForm({
    mode: "uncontrolled",
  });

  const handleSubmit = async (data: CreateFoodRequest) => {
    try {
      const res = await api.post(`/api/request/${food.id}`, {
        body: JSON.stringify(data),
      });
      if (res.ok) {
        toast.success("Request made successfully.");
        close();
      } else {
        toast.error("Request error");
      }
    } catch {
      toast.error("Request error");
    }
  };

  useEffect(() => {
    if (!food) return;
    form.initialize({
      name: food.name,
    });
  }, [form, food]);

  if (!form.initialized) return;

  return (
    <>
      <Modal
        opened={opened}
        onClose={close}
        title="Request Food"
        overlayProps={{
          backgroundOpacity: 0.55,
          blur: 3,
        }}
      >
        <TextInput
          disabled
          {...form.getInputProps("name")}
          key={form.key("name")}
          mt="md"
          label="Name"
        />
        <form onSubmit={form.onSubmit(handleSubmit)}>
          <NumberInput
            {...form.getInputProps("quantity")}
            required
            allowNegative={false}
            allowDecimal={false}
            key={form.key("quantity")}
            mt="md"
            label="Quantity"
            placeholder="Quantity"
          />
          <NumberInput
            {...form.getInputProps("donation")}
            required
            allowNegative={false}
            allowDecimal={false}
            key={form.key("donation")}
            mt="md"
            label="Donation"
            placeholder="Donation"
          />
          <Button type="submit" mt="md">
            Submit
          </Button>
        </form>
      </Modal>
    </>
  );
};

export default RequestFood;
