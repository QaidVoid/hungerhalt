import { Navigate, Outlet, createBrowserRouter, useLocation } from "react-router-dom";
import Layout from "./components/layout";
import Foods from "./pages/foods";
import ManageFoods from "./pages/manage-foods";
import { useAuth } from "./context/auth";
import Login from "./pages/login";
import PersistLogin from "./components/persist-login";
import FoodRequests from "./pages/requests";
import Dashboard from "./components/admin/dashboard";
import UserFoods from "./components/admin/user-foods";
import Users from "./components/admin/users";

const AuthGuard = ({ roles }: { roles: string[] }) => {
  const { user } = useAuth();
  const location = useLocation();

  return roles.includes(user?.role) ? (
    <Outlet />
  ) : user?.id ? (
    <div className="flex flex-col justify-center w-full items-center text-2xl">
      <span>Unauthorized</span>
      <span className="text-gray-400 text-md">You're not authorized to view this page.</span>
    </div>
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  );
};

export const router = createBrowserRouter([
  {
    path: "/admin",
    element: <AuthGuard roles={["Admin"]} />,
    children: [
      {
        path: "/admin",
        element: <Dashboard />,
        children: [
          {
            path: "/admin/foods",
            element: <UserFoods />,
          },
          {
            path: "/admin/users",
            element: <Users />,
          },
        ],
      },
    ],
  },
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <PersistLogin />,
        children: [
          {
            path: "/login",
            element: <Login />,
          },
          {
            path: "/",
            element: <AuthGuard roles={["User"]} />,
            children: [
              {
                path: "/foods",
                element: <Foods />,
              },
              {
                path: "/manage-foods",
                element: <ManageFoods />,
              },
              {
                path: "/requests",
                element: <FoodRequests />,
              },
            ],
          },
        ],
      },
    ],
  },
]);
