import { createContext, useState, useContext, type PropsWithChildren } from "react";
import type { AuthUser } from "../types";
import { api } from "../api";

interface AuthContextType {
  authenticated: boolean;
  user: AuthUser;
  login: (user: AuthUser) => void;
  logout: () => void;
}

const AuthContext = createContext<AuthContextType>({} as AuthContextType);

export const AuthProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const [authenticated, setAuthenticated] = useState(false);
  const [user, setUser] = useState<AuthUser>({ id: 0, role: "" });

  const login = (user: AuthUser) => {
    setAuthenticated(true);
    setUser(user);
  };

  const logout = async () => {
    await api.post("/api/auth/logout");
    setAuthenticated(false);
    setUser({ id: 0, role: "" });
  };

  const contextData = {
    authenticated,
    user,
    login,
    logout,
  };

  return <AuthContext.Provider value={contextData}>{children}</AuthContext.Provider>;
};

export const useAuth = () => useContext(AuthContext);
