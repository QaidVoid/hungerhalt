import { faker } from "@faker-js/faker";
import { ApprovalStatus, PrismaClient, RequestStatus } from "@prisma/client";
import argon2 from "argon2";

const prisma = new PrismaClient();

const numberOfUsers = 10;
const numberOfFoodItemsPerUser = 10;
const numberOfRequestsPerFoodItem = 10;

async function main() {
  const password = await argon2.hash("password123");
  const assignedFoodIdsForEachUser = {};

  const newAdminUser = await prisma.user.create({
    data: {
      name: "Admin User",
      email: "admin@example.com",
      password: password,
      role: "Admin",
    },
  });

  await Promise.all([
    ...Array(numberOfUsers)
      .fill(null)
      .map(async (_, index) => {
        const newUser = await prisma.user.create({
          data: {
            name: faker.person.fullName(),
            email: faker.internet.email(),
            password: password,
            role: "User",
          },
        });

        await Promise.all([
          ...Array(Math.floor(Math.random() * numberOfFoodItemsPerUser))
            .fill(null)
            .map(async (_) => {
              let newFood;
              do {
                newFood = await prisma.food.create({
                  data: {
                    name: [
                      faker.food.vegetable,
                      faker.food.meat,
                      faker.food.fruit,
                      faker.food.dish,
                    ].at(Math.floor(Math.random() * 4))!(),
                    quantity: Math.floor(Math.random() * 20) + 1,
                    approvalStatus:
                      [ApprovalStatus.Pending, ApprovalStatus.Accepted, ApprovalStatus.Rejected].at(
                        Math.floor(Math.random() * 3),
                      ) ?? ApprovalStatus.Pending,
                    postedAt: new Date(),
                    expiresAt: new Date(
                      new Date().setDate(new Date().getDate() + Math.floor(Math.random() * 30)),
                    ),
                    pickupLocation: faker.location.city(),
                    user: { connect: { id: Math.floor(Math.random() * newUser.id + 1) } },
                  },
                });
              } while (assignedFoodIdsForEachUser[newUser.id]?.includes(newFood.id) || !newFood);

              await prisma.request.createMany({
                data: [
                  ...Array(Math.floor(Math.random() * numberOfRequestsPerFoodItem))
                    .fill(null)
                    .map((_) => ({
                      donation: Math.floor(Math.random() * 10) + 1,
                      quantity: Math.floor(Math.random() * 5) + 1,
                      requestedAt: new Date(),
                      status: [
                        RequestStatus.Pending,
                        RequestStatus.Accepted,
                        RequestStatus.Rejected,
                      ].at(Math.floor(Math.random() * 3)),
                      userId: Math.floor(Math.random() * newUser.id + 1),
                      foodId: Math.floor(Math.random() * newUser.id + 1),
                    })),
                ],
              });

              assignedFoodIdsForEachUser[newUser.id]?.push(newFood.id);
            }),
        ]);
      }),
  ]);
}
main()
  .then(async () => {
    console.log("Seeding completed successfully.");
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
