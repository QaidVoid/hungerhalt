/*
  Warnings:

  - You are about to drop the column `foodImage` on the `Food` table. All the data in the column will be lost.
  - You are about to drop the column `foodName` on the `Food` table. All the data in the column will be lost.
  - You are about to drop the column `foodQuantity` on the `Food` table. All the data in the column will be lost.
  - Added the required column `image` to the `Food` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `Food` table without a default value. This is not possible if the table is not empty.
  - Added the required column `quantity` to the `Food` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Food" DROP COLUMN "foodImage",
DROP COLUMN "foodName",
DROP COLUMN "foodQuantity",
ADD COLUMN     "image" TEXT NOT NULL,
ADD COLUMN     "name" TEXT NOT NULL,
ADD COLUMN     "quantity" INTEGER NOT NULL;
