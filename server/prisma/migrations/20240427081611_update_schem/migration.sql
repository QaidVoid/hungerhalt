/*
  Warnings:

  - Added the required column `postedAt` to the `Food` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Food" ADD COLUMN     "postedAt" TIMESTAMP(3) NOT NULL;
