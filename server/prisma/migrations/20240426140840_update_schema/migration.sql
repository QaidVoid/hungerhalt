-- CreateEnum
CREATE TYPE "ApprovalStatus" AS ENUM ('Pending', 'Accepted', 'Rejected');

-- AlterEnum
ALTER TYPE "RequestStatus" ADD VALUE 'Rejected';

-- AlterTable
ALTER TABLE "Food" ADD COLUMN     "approvalStatus" "ApprovalStatus" NOT NULL DEFAULT 'Pending';
