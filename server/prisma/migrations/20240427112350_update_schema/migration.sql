/*
  Warnings:

  - You are about to drop the column `image` on the `Food` table. All the data in the column will be lost.
  - You are about to drop the column `deliveryStatus` on the `Request` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Food" DROP COLUMN "image";

-- AlterTable
ALTER TABLE "Request" DROP COLUMN "deliveryStatus";

-- DropEnum
DROP TYPE "DeliveryStatus";
