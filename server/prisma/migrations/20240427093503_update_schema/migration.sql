/*
  Warnings:

  - You are about to drop the column `deliveryStatus` on the `Food` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Food" DROP COLUMN "deliveryStatus";

-- AlterTable
ALTER TABLE "Request" ADD COLUMN     "deliveryStatus" "DeliveryStatus" NOT NULL DEFAULT 'Available';
