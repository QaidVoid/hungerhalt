import jwt from "jsonwebtoken";
import { ACCESS_TOKEN_SECRET } from "./constants";
import db from "./db";

export const authenticateUser = async (req, res, next) => {
  try {
    const token = req.cookies.token;

    if (!token) {
      return res.status(401).json({ error: 'Unauthorized - Token not provided' });
    }

    const decoded = jwt.verify(token, ACCESS_TOKEN_SECRET);

    if (!decoded.userId) {
      return res.status(401).json({ error: 'Unauthorized - Invalid token' });
    }

    const user = await db.user.findUnique({
      where: {
        id: decoded.userId,
      },
      select: {
        id: true,
        role: true
      }
    });

    if (!user) {
      return res.status(401).json({ error: 'Unauthorized - User not found' });
    }

    req.user = user;
    next();
  } catch (error) {
    console.error('Error authenticating user:', error);
    return res.status(500).json({ error: 'Internal server error' });
  }
};
