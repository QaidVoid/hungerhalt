import "dotenv/config";

export const PORT = process.env.PORT || 3000;

export const ACCESS_TOKEN_SECRET = process.env.ACCESS_TOKEN_SECRET || "default";
