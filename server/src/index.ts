import express from "express";
import cookieParser from "cookie-parser";
import path from "node:path";
import routes from "./routes";
import { PORT } from "./constants";

const app = express();

app.use(express.static(path.join(__dirname, "client")));

app.use(express.json());
app.use(cookieParser());
app.use("/api", routes);

app.get("*", (_req, res) => {
  res.sendFile(path.join(__dirname, "client/index.html"));
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
