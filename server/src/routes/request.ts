import { Router } from "express";
import db from "../db";

const router = Router();

router.get("/", async (req, res) => {
  const userId = req.user.id;

  try {
    const myRequests = await db.request.findMany({
      where: {
        userId: userId,
      },
      include: {
        food: true,
      },
    });

    res.status(200).json(myRequests);
  } catch (error) {
    console.error("Error fetching my requests:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/manage", async (req, res) => {
  const userId = req.user.id;

  try {
    const foods = await db.food.findMany({
      where: {
        userId: userId
      },
      include: {
        request: {
          include: {
            food: true,
            user: true
          }
        }
      },
    });

    res.status(200).json(foods.flatMap(req => req.request));
  } catch (error) {
    console.error("Error fetching my requests:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/:requestId", async (req, res) => {
  try {
    const foodId = Number.parseInt(req.params.foodId);
    const userId = req.user.id; // Get user ID from authenticated user

    const foodItem = await db.food.findFirst({
      where: {
        id: foodId,
        approvalStatus: "Accepted",
      },
    });

    if (!foodItem) {
      return res.status(404).json({ error: "Food item not found" });
    }

    const request = await db.request.create({
      data: {
        donation: req.body.donation || 0,
        quantity: req.body.quantity || 1,
        requestedAt: new Date(),
        status: "Pending",
        userId: userId,
        foodId: foodId,
      },
    });

    res.status(201).json({ message: "Food requested successfully", request: request });
  } catch (error) {
    console.error("Error requesting food:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.put("/accept/:requestId", async (req, res) => {
  try {
    const requestId = Number.parseInt(req.params.requestId);
    const userId = req.user.id; // Get user ID from authenticated user

    const request = await db.request.findFirst({
      where: {
        id: requestId,
        food: {
          userId: userId,
        },
      },
    });

    if (!request) {
      return res.status(404).json({ error: "Request not found" });
    }

    const available = await db.food.findFirst({
      where: {
        id: request.foodId,
        quantity: {
          gte: request.quantity,
        },
      },
    });

    if (!available) {
      return res.status(404).json({ error: "Food with specified quantity is not available" });
    }

    const updatedRequest = await db.request.update({
      where: {
        id: requestId,
      },
      data: {
        status: "Accepted",
      },
    });

    await db.food.update({
      where: {
        id: request.foodId,
      },
      data: {
        quantity: {
          decrement: request.quantity,
        },
      },
    });

    res.status(200).json({ message: "Request accepted successfully", request: updatedRequest });
  } catch (error) {
    console.error("Error accepting request:", error);

    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/:requestId", async (req, res) => {
  try {
    const requestId = Number.parseInt(req.params.requestId);
    const userId = req.user.id; // Get user ID from authenticated user

    const foodToDelete = await db.request.findFirst({
      where: {
        id: requestId,
        userId: userId,
      },
    });

    if (!foodToDelete) {
      return res.status(404).json({ error: "Request not found" });
    }

    await db.request.delete({
      where: {
        id: requestId,
      },
    });

    res.status(200).json({ message: "Request deleted successfully" });
  } catch (error) {
    console.error("Error deleting request:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

export default router;
