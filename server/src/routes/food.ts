import { Router } from "express";
import db from "../db";

const router = Router();

router.get('/all', async (req, res) => {
  try {
    const allFoods = await db.food.findMany({
      where: {
        approvalStatus: "Accepted"
      }
    });

    res.status(200).json(allFoods);
  } catch (error) {
    console.error('Error fetching all foods:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

router.get('/recent', async (req, res) => {
  try {
    const allFoods = await db.food.findMany({
      where: {
        approvalStatus: "Accepted"
      },
      orderBy: {
        id: "desc"
      },
      include: {
        user: true
      }
    });

    res.status(200).json(allFoods);
  } catch (error) {
    console.error('Error fetching all foods:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

router.get('/mine', async (req, res) => {
  try {
    const userId = req.user.id; // Retrieve user ID from authenticated user

    const userFoods = await db.food.findMany({
      where: {
        userId: userId,
      },
      include: {
        request: {
          include: {
            user: true
          }
        }
      }
    });

    res.status(200).json(userFoods);
  } catch (error) {
    console.error('Error fetching user foods:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

router.post("/", async (req, res) => {
  try {
    const { name, quantity, expiresAt, pickupLocation } = req.body;
    const userId = req.user.id;

    const newFood = await db.food.create({
      data: {
        name: name,
        quantity: quantity,
        postedAt: new Date(),
        expiresAt: new Date(expiresAt),
        pickupLocation: pickupLocation,
        userId: userId,
      },
    });

    res.status(201).json({ message: "Food added successfully", food: newFood });
  } catch (error) {
    console.error("Error adding food:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/:foodId", async (req, res) => {
  try {
    const foodId = Number.parseInt(req.params.foodId);
    const userId = req.user.id; // Get user ID from authenticated user

    const foodItem = await db.food.findFirst({
      where: {
        id: foodId,
        userId: userId
      },
    });

    if (!foodItem) {
      return res.status(404).json({ error: "Food item not found" });
    }

    res.status(200).json({ food: foodItem });
  } catch (error) {
    console.error("Error fetching food by ID:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.put("/:foodId", async (req, res) => {
  try {
    const foodId = Number.parseInt(req.params.foodId);
    const userId = req.user.id;
    const { name, quantity, expiresAt, pickupLocation } =
      req.body;

    const existingFood = await db.food.findFirst({
      where: {
        id: foodId,
        userId: userId,
      },
    });

    if (!existingFood) {
      return res.status(404).json({ error: "Food item not found" });
    }

    const updatedFood = await db.food.update({
      where: {
        id: foodId,
      },
      data: {
        name: name || existingFood.name,
        quantity: quantity || existingFood.quantity,
        expiresAt: expiresAt ? new Date(expiresAt) : existingFood.expiresAt,
        pickupLocation: pickupLocation || existingFood.pickupLocation,
        approvalStatus: "Pending"
      },
    });

    res.status(200).json({ message: "Food item updated successfully", food: updatedFood });
  } catch (error) {
    console.error("Error updating food:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/:foodId", async (req, res) => {
  try {
    const foodId = Number.parseInt(req.params.foodId);
    const userId = req.user.id; // Get user ID from authenticated user

    const foodToDelete = await db.food.findFirst({
      where: {
        id: foodId,
        userId: userId,
      },
    });

    if (!foodToDelete) {
      return res.status(404).json({ error: "Food item not found" });
    }

    await db.food.delete({
      where: {
        id: foodId,
      },
    });

    res.status(200).json({ message: "Food item deleted successfully" });
  } catch (error) {
    console.error("Error deleting food:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

export default router;
