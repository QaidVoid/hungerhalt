import { Router } from "express";
import { authenticateUser } from "../middleware";
import auth from "./auth";
import food from "./food";
import request from "./request";
import user from "./user";
import admin from "./admin";

const router = Router();

router.use("/auth", auth);

router.use(authenticateUser);

router.use("/food", food);

router.use("/request", request);

router.use("/user", user);

router.use("/admin", admin);

export default router;
