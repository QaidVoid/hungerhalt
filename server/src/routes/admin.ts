import { Router } from "express";
import db from "../db";

const router = Router();

router.get("/food/all", async (req, res) => {
  try {
    const isAdmin = req.user.role === "Admin";

    if (!isAdmin) {
      return res.status(403).json({ error: "Only admins can access this route." });
    }

    const allFoods = await db.food.findMany();

    res.status(200).json(allFoods);
  } catch (error) {
    console.error("Error fetching all foods:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.put("/food/:foodId", async (req, res) => {
  try {
    const isAdmin = req.user.role === "Admin";

    if (!isAdmin) {
      return res.status(403).json({ error: "Only admins can accept or reject food items" });
    }

    const { foodId } = req.params;
    const { approvalStatus } = req.body;

    if (!["Accepted", "Rejected"].includes(approvalStatus)) {
      return res.status(400).json({ error: "Invalid approval status" });
    }

    const updatedFood = await db.food.update({
      where: {
        id: Number.parseInt(foodId),
      },
      data: {
        approvalStatus: approvalStatus,
      },
    });

    res
      .status(200)
      .json({ message: "Food item approval status updated successfully", food: updatedFood });
  } catch (error) {
    console.error("Error updating food item approval status:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});


router.get("/users/all", async (req, res) => {
  try {
    const isAdmin = req.user.role === "Admin";

    if (!isAdmin) {
      return res.status(403).json({ error: "Only admins can access this route." });
    }

    const users = await db.user.findMany();

    res.status(200).json(users);
  } catch (error) {
    console.error("Error fetching all users:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

export default router;
