import { Router } from "express";
import jwt from "jsonwebtoken";
import argon2 from "argon2";
import { ACCESS_TOKEN_SECRET } from "../constants";
import db from "../db";
import { authenticateUser } from "../middleware";

const router = Router();

router.get("/verify", authenticateUser, async (req, res) => {
  res.status(200).json(req.user);
});

router.post("/logout", authenticateUser, async (req, res) => {
  res.clearCookie("token");
  res.status(204).json();
});

router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await db.user.findFirst({
      where: {
        email,
      },
    });

    if (!user) {
      return res.status(401).json({ error: "Invalid email or password" });
    }

    const passwordMatch = await argon2.verify(user.password, password);

    if (!passwordMatch) {
      return res.status(401).json({ error: "Invalid email or password" });
    }

    const token = jwt.sign({ userId: user.id }, ACCESS_TOKEN_SECRET, {
      expiresIn: "1d",
    });

    res.status(200).json({ message: "Login successful", user, token });
  } catch (error) {
    console.error("Error logging in:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/register", async (req, res) => {
  try {
    const { name, email, password } = req.body;

    const existingUser = await db.user.findFirst({
      where: {
        email: email,
      },
    });

    if (existingUser) {
      return res.status(400).json({ error: "Email already exists" });
    }

    const hashedPassword = await argon2.hash(password);

    const newUser = await db.user.create({
      data: {
        name: name,
        email: email,
        password: hashedPassword,
      },
    });

    const token = jwt.sign({ userId: newUser.id }, ACCESS_TOKEN_SECRET, {
      expiresIn: "1d",
    });

    res.status(201).json({ message: "User registered successfully", user: newUser, token });
  } catch (error) {
    console.error("Error registering user:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

export default router;
