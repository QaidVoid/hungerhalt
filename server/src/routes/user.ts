import { Router } from "express";
import db from "../db";

const router = Router();

router.get('/:userId/foods', async (req, res) => {
  try {
    const userId = Number.parseInt(req.params.userId);

    const userFoods = await db.food.findMany({
      where: {
        userId: userId,
      },
    });

    res.status(200).json({ foods: userFoods });
  } catch (error) {
    console.error('Error fetching user foods:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

export default router;
